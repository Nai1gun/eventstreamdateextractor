package com.nailgun.test;

/**
 * @author nailgun
 * @since 17.06.13
 */
public class DateExtractionRegexps {

    public static final String RE_MONTHS = ReMonths.regexpGroup();

    public static final String RE_DAY = "(?<day>\\d\\d)";

    public static final String RE_WEEK_DAY = "(?<weekDay>понедельник|вторник|среда|четверг|пятница|суббота|воскресенье)";

    public static final String RE_HOURS = "(?<hour>\\d\\d)";

    public static final String RE_MINS = "(?<min>\\d\\d)";

    public static final String REGEXP_DATE_1 = "(?<dateTime>" + RE_DAY + " " + RE_MONTHS + " " + RE_WEEK_DAY + " "
        + RE_HOURS + ":" + RE_MINS + ")";

    public static final String REGEXP_DATE_2 = "(?<date>" + RE_DAY + " " + RE_MONTHS + " " + RE_WEEK_DAY + ").*"
           + "НАЧАЛО (?<time>" + RE_HOURS + ":" + RE_MINS + ")";

}
