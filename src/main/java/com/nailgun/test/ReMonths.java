package com.nailgun.test;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author nailgun
 * @since 18.06.13
 */
public enum ReMonths {

    JANUARY("января", 1), FEBRUARY("февраля", 2), MARCH("марта", 3), APRIL("апреля", 4),
    MAY("мая", 5), JUNE("июня", 6), JULY("июля", 7), AUGUST("августа", 8),
    SEPTEMBER("сентября", 9), OCTOBER("октября", 10), NOVEMBER("ноября", 11), DECEMBER("декабря", 12);

    private static Map<String, ReMonths> index = new HashMap<>();

    static {
        for(ReMonths month: values()) {
            index.put(month.getNameRu(), month);
        }
    }

    private ReMonths(String nameRu, int number) {
        this.nameRu = nameRu;
        this.number = number;
    }

    private String nameRu;

    private int number;

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public int getNumber() {
        return number;
    }

    public static ReMonths valueByRuName(String ruName) {
        return index.get(ruName);
    }

    public static List<String> nameRuValues() {
        List<String> ret = new ArrayList<>();
        for (ReMonths month: values()) {
            ret.add(month.getNameRu());
        }
        return ret;
    }

    public static String regexpGroup() {
        return "(?<month>" + StringUtils.join(nameRuValues(), "|") + ")";
    }
}
