package com.nailgun.test;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.ISODateTimeFormat;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author nailgun
 * @since 25.06.13
 */
public class DateExtractor {

    private static final String TIMEML_TEMPLATE =
            "<?xml version=\"1.0\"?>\n" +
                    "<!DOCTYPE TimeML SYSTEM \"TimeML.dtd\">\n" +
                    "<TimeML>\n" +
                    "%s\n" +
                    "</TimeML>";

    private String regex;

    private String text;

    private String timeMl;

    private Map<String, InstantHolder<?>> matches;

    private List<DateTime> extracted;

    public DateExtractor(String regex, String text) {
        this.regex = regex;
        this.text = text;
    }

    public List<DateTime> extractDateTime() {
        if (extracted == null) {
            doExtractAndMapDates();
        }
        return extracted;
    }

    public String extractTimeMl() {
        if (matches == null) {
            doExtractAndMapDates();
        }
        if (timeMl == null) {
            doExtractTimeMl();
        }
        return timeMl;
    }

    private void doExtractAndMapDates() {
        matches = new LinkedHashMap<>();
        extracted = new ArrayList<>();
        System.out.println(text);
        System.out.println(regex);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        int i = 1;
        while(matcher.find()) {

            int year, month, day, hour, minute;

            if (regex.contains("?<year>")) {
                year = Integer.parseInt(matcher.group("year"));
            } else {
                year = new DateTime().getYear();
            }

            if (regex.contains("?<month>")) {
                String monthName = matcher.group("month");
                month = ReMonths.valueByRuName(monthName).getNumber();
            } else throw new RuntimeException();

            if (regex.contains("?<day>")) {
                day = Integer.parseInt(matcher.group("day"));
            } else throw new RuntimeException();

            if (regex.contains("?<hour>")) {
                hour = Integer.parseInt(matcher.group("hour"));
            } else throw new RuntimeException();

            if (regex.contains("?<min>")) {
                minute = Integer.parseInt(matcher.group("min"));
            } else throw new RuntimeException();
            DateTime dateTime = new DateTime(year, month, day, hour, minute);
            extracted.add(dateTime);
            LocalDate localDate = new LocalDate(year, month, day);
            LocalTime localTime = new LocalTime(hour, minute);

            int dateId = 0;
            try {
                String match = matcher.group("dateTime");
                matches.put(match, new InstantHolder(dateTime, i++));
            } catch (Exception e) {}

            try {
                String match = matcher.group("date");
                matches.put(match, new InstantHolder(localDate, dateId = i++));
            } catch (Exception e) {}

            try {
                String match = matcher.group("time");
                matches.put(match, new InstantHolder(localTime, i++, dateId));
            } catch (Exception e) {}

        }
    }

    private void doExtractTimeMl() {
        String timeMl = text;
        for (Map.Entry<String, InstantHolder<?>> entry: matches.entrySet()) {
            InstantHolder<?> val = entry.getValue();
            String formatted =
                val.instant instanceof DateTime ?
                    ((DateTime)val.instant).toString(ISODateTimeFormat.dateHourMinute()):
                    val.instant instanceof LocalDate ?
                        ((LocalDate)val.instant).toString(ISODateTimeFormat.date()):
                        val.instant instanceof LocalTime ?
                            "T" + ((LocalTime)val.instant).toString(ISODateTimeFormat.hourMinute()):
                            "";
            timeMl = StringUtils.replace(timeMl, entry.getKey(),
                String.format("<TIMEX3 tid=\"t%s\" type=\"DATE\" value=\"%s\"" +
                    (entry.getValue().linkId != 0 ? String.format(" anchorTimeID=\"t%s\"", entry.getValue().linkId) : "") +
                    ">%s</TIMEX3>",
                    val.id, formatted, entry.getKey()));
        }
        timeMl = String.format(TIMEML_TEMPLATE, timeMl);
        this.timeMl = timeMl;

    }

    private static class InstantHolder<T> {

        T instant;

        int id;

        int linkId;

        InstantHolder(T instant, int id, int linkId) {
            this.instant = instant;
            this.id = id;
            this.linkId = linkId;
        }

        InstantHolder(T instant, int id) {
            this(instant, id, 0);
        }
    }

}
