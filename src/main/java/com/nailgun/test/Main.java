package com.nailgun.test;

import org.joda.time.DateTime;
import java.util.List;

/**
 * @author nailgun
 * @since 18.06.13
 */
public class Main {

    public static void main(String[] args) {
        String text = "Семинар Как открыть пиццерию 18 июня вторник 11:00 бесплатно 16 мая четверг 10:31";
//        String text = "24 июня понедельник 6 НЕ ИДУ " +
//                "Марк Ротко — Джеймс Таррелл. Метафизическая абстракция на холсте и без холста " +
//                "Добавил garageccc 5 марта в 18:41 " +
//                "НАЧАЛО 19:30 " +
//                "АДРЕСМосква, Парк Горького, у Пионерского пруда, ул. Крымский Вал, д. 9, стр. 4 " +
//                "ЦЕНА бесплатно";
        DateExtractor dex = new DateExtractor(DateExtractionRegexps.REGEXP_DATE_1, text);//use REGEXP_DATE_2 for the second text
        String timeMl = dex.extractTimeMl();
        List<DateTime> dts = dex.extractDateTime();
        for (DateTime dt: dts) {
            System.out.println(dt);
        }
        System.out.println(timeMl);
    }
}
